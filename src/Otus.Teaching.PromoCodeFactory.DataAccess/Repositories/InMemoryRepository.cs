﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T item)
        {
            var tempData = Data.ToList();
            tempData.Add(item);
            Data = tempData.AsEnumerable<T>();

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id).Id);
        }

        public Task<Guid> UpdateAsync(T item)
        {
            var tempData = Data.ToList();
            if (tempData.Remove(Data.FirstOrDefault(x => x.Id == item.Id)) && tempData.FirstOrDefault(x => x.Id == item.Id) == null)
                tempData.Add(item);
            Data = tempData.AsEnumerable<T>();

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id) == null ? Guid.Empty : Data.FirstOrDefault(x => x.Id == item.Id).Id);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var tempData = Data.ToList();
            if(!tempData.Remove(Data.FirstOrDefault(x => x.Id == id))) return Task.FromResult(false);
            Data = tempData.AsEnumerable<T>();

            return Task.FromResult(true);
        }
    }
}